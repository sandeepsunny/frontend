import { TestBed } from '@angular/core/testing';

import { AdminCRUDService } from './admin-crud.service';

describe('AdminCRUDService', () => {
  let service: AdminCRUDService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminCRUDService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
