// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { AuthenticationService } from '../authentication.service';
// import { ShowJobsComponent } from '../show-jobs/show-jobs.component';

// @Component({
//   selector: 'app-company-registration-form',
//   templateUrl: './company-registration-form.component.html',
//   styleUrls: ['./company-registration-form.component.css']
// })
// export class CompanyRegistrationFormComponent implements OnInit{
//   ngOnInit(): void {
   
//   }
//   constructor(private router:Router,private auth:AuthenticationService){}
//   Register( applicationForm:any)
//   {
//     applicationForm.id=localStorage.getItem("id");
//     applicationForm.company=localStorage.getItem("company");
//     applicationForm.role=localStorage.getItem("role");
//     console.log(applicationForm);
//     return this.auth.submitForm(applicationForm.id,applicationForm.company,applicationForm.role,applicationForm).subscribe((data: any) => {
//       console.log(data);
//        this.router.navigateByUrl("showjobs");
      
//     });
    
//   }

  

// }
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService
 } from 'ngx-toastr';
import { User } from 'src/User';
import { AuthenticationService } from '../authentication.service';
import { ShowJobsComponent } from '../show-jobs/show-jobs.component';

@Component({
  selector: 'app-company-registration-form',
  templateUrl: './company-registration-form.component.html',
  styleUrls: ['./company-registration-form.component.css']
})
export class CompanyRegistrationFormComponent implements OnInit{
  user!: User;
  ngOnInit(): void {
   
  }
  constructor(private router:Router,private auth:AuthenticationService,private toaster: ToastrService){}
  showToder4()
  {
    this.toaster.success('Company Applied Successfully','Successfully')
  }
  Register( applicationForm:any)
  {
   
    applicationForm.id=localStorage.getItem("id");
    applicationForm.company=localStorage.getItem("company");
    applicationForm.role=localStorage.getItem("role");
    console.log(applicationForm);
    return this.auth.submitForm(applicationForm.id,applicationForm.company,applicationForm.role,applicationForm).subscribe((data: any) => {
      console.log(data);
       this.router.navigateByUrl("showjobs"); 
    });
    

  }

  

}
