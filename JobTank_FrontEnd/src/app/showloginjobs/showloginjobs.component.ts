import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-showloginjobs',
  templateUrl: './showloginjobs.component.html',
  styleUrls: ['./showloginjobs.component.css']
})
export class ShowloginjobsComponent {

  job:any
  ngOnInit(): void {
    this.getAllJobs();
  }
  constructor(
    private auth:AuthenticationService, private router:Router) { }

getAllJobs()
{
 return this.auth.getAlljobs().subscribe((j:any)=>
 {
  console.log(j);
  this.job=j;
 }
 )};

  addToFavorite() {
    Swal.fire('ERROR', ' First login to apply for job', 'error');
  //  alert("login to apply");
    this.router.navigateByUrl("login");
}
}

