import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';


@Component({
  selector: 'app-categorysearch',
  templateUrl: './categorysearch.component.html',
  styleUrls: ['./categorysearch.component.css']
})
export class CategorysearchComponent implements OnInit {
  job:any;
  name:any;
  ngOnInit(): void {
  
  }
  constructor(
         private auth:AuthenticationService, private router:Router) { }

    
    getByCategory():any{
    

    this.auth.getByCategory(this.name).subscribe(
      {
        
        next:(data:any) => {
          if(data.length==0){
            alert("sorry no results found")
          }
         this.job=data;
                  console.log(data);
                

        }
      }
    );

    //
    
    //


  }
  addToFavorite(id:number,company:string,role:string,job: any) {
    console.log(job);
    job.username=localStorage.getItem("username");
    job.jobid=localStorage.getItem("id");
    this.auth.appiledJob(job.jobid,job.username,job).subscribe((job1:any) => {
      console.log(job1);
      localStorage.setItem("id",id.toString());
      localStorage.setItem("company",company.toString());
      localStorage.setItem("role",role.toString());
      this.router.navigateByUrl("companyRegistration");
    
  
  });
  }

}
