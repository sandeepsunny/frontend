import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-show-jobs',
  templateUrl: './show-jobs.component.html',
  styleUrls: ['./show-jobs.component.css']
})
export class ShowJobsComponent implements OnInit {
job:any
  
  ngOnInit(): void {
    this.getAllJobs();
  }
  constructor(
    private auth:AuthenticationService, private router:Router) { }

getAllJobs()
{
 return this.auth.getAlljobs().subscribe((j:any)=>
 {
  console.log(j);
  this.job=j;
 }
 )};

//   addToFavorite(id:number,company:string,role:string,job: any) {
//     console.log(job);
//     this.auth.appiledJob(job).subscribe((job1:any) => {
//       console.log(job1);
//       localStorage.setItem("id",id.toString());
//       localStorage.setItem("company",company.toString());
//       localStorage.setItem("role",role.toString());
//       this.router.navigateByUrl("companyRegistration");
    

//   });
// }
addToFavorite(id:number,company:string,role:string,job: any) {
  console.log(job);
  job.username=localStorage.getItem("username");
  job.jobid=localStorage.getItem("id");
  this.auth.appiledJob(job.jobid,job.username,job).subscribe((job1:any) => {
    console.log(job1);
    localStorage.setItem("id",id.toString());
    localStorage.setItem("company",company.toString());
    localStorage.setItem("role",role.toString());
    this.router.navigateByUrl("companyRegistration");
  

});
}

}

  

