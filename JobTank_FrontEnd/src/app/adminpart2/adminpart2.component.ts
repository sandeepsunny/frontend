import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminCRUDService } from '../admin-crud.service';

@Component({
  selector: 'app-adminpart2',
  templateUrl: './adminpart2.component.html',
  styleUrls: ['./adminpart2.component.css']
})
export class Adminpart2Component implements OnInit{
  job:any;
  ngOnInit(): void {
    this.getAllJobs();
  }
  constructor( private crud:AdminCRUDService,private router:Router,private toaster: ToastrService)
  {
   
  }
  showToaster6(){
    this.toaster.success('Selected, mail Sent Successfully','Sent')
  }
  showToaster7(){
    this.toaster.error('Rejected, mail Sent Successfully','Sent')
  }
  getAllJobs()
  {
   return this.crud.getAllRegistered().subscribe((j:any)=>
   {
    console.log(j);
    this.job=j;
   }
   )};
  
  //  sendEmail(userName:any){
  //  // console.log(userName);
  //   this.crud.sendEmail(userName).subscribe(()=>
  //   {
  //     console.log("sharath");
  //   }
  //   )
  //  }
  //  sendEmail1(userName:any){
  //   // console.log(userName);
  //    this.crud.sendEmail1(userName).subscribe(()=>
  //    {
  //      console.log("sharath");
  //    }
  //    )
  //   }
  sendEmail(id:any){
      console.log(id);
      this.crud.sendEmail(id).subscribe(()=>
      {
        console.log("sharath");
      }
      )
     }
     sendEmail1(id:any){
      // console.log(userName);
       this.crud.sendEmail1(id).subscribe(()=>
       {
         console.log("sharath");
       }
       )
      }
}