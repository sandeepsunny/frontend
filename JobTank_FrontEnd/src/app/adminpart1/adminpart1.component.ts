import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminCRUDService } from 'src/admin-crud.service';


@Component({
  selector: 'app-adminpart1',
  templateUrl: './adminpart1.component.html',
  styleUrls: ['./adminpart1.component.css']
})
export class Adminpart1Component implements OnInit
{
  showAdd: boolean=false;
  job: any;
  ngOnInit(): void {
   this.getAllJobs();
  }
  constructor( private crud:AdminCRUDService,private router:Router,private toaster: ToastrService)
  {
   
  }

  showToatr1(){
      this.toaster.success('Company Added Successfully','Added')
  }
  showToatr2(){
    this.toaster.error('Company Deleted Successfully','Deleted')
}
showToatr3(){
  this.toaster.error('Company Updated Successfully','Updated')
}
  onAdd(loginForm: any) {
    console.log(loginForm);
  
    this.crud.addJob(loginForm).subscribe((data: any) => {
      console.log(data);
      this.ngOnInit();
    });
    this.showAdd =false;
  }
  getAllJobs()
{
 return this.crud.getAllJobs().subscribe((j:any)=>
 {
  console.log(j);
  this.job=j;
 }
 )};

 delete(id:any)
{
 
   return  this.crud.delete1(id).subscribe((j:any) => {
     this.ngOnInit();
     console.log(j);
    });


}
update(id:number,name:string,location:string,company:string,role:string,job:any)
{
  localStorage.setItem("id",id.toString());
  localStorage.setItem("name",name.toString());
  localStorage.setItem("location",location.toString());
  localStorage.setItem("company",company.toString());
  localStorage.setItem("role",role.toString());

this.router.navigateByUrl("update");
}

}
