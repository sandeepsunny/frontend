import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Adminpart1Component } from './adminpart1.component';

describe('Adminpart1Component', () => {
  let component: Adminpart1Component;
  let fixture: ComponentFixture<Adminpart1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Adminpart1Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Adminpart1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
