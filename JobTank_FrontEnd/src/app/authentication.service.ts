import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/User';

const USER_NAME ="name"
@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  getByCategory(name:any){
    console.log(name);
   
    return this.httpClient.get("/job/search"+"/"+name);
    
  }

   
  updateProfile(user: User) {
    console.log(user.userName);

    return this.httpClient.put("/user/update"+"/"+user.userName,user);
  }

  user2!: User;
  


  registerUser(newUser: any) {
    this.user2 = new User();
    this.user2.userName = newUser.userName;
    this.user2.password = newUser.password;
    this.user2.email = newUser.email;
    this.user2.qualification = newUser.qualification;
 
    return this.httpClient.post('/user/list', this.user2);
  }

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  getAlljobs()  {
    return this.httpClient.get("/job/list");
  }
  cancelJob(id:any){
    console.log(id);
    return  this.httpClient.delete("/AppliedJobs/delete"+"/"+id); 

  }

  appiledJob(jobid:any,username:any,job:any)  {

    return  this.httpClient.post("/AppliedJobs/list1", job);    
  }
  delete1(id:number)
  {
    return this.httpClient.delete("/job/delete/"+id);
  }

updateProduct(id:any,job:any)
{
 return this.httpClient.put("/products/list/",job);
}
submitForm(id:any,company:any,role:any,job:any)
{
 return this.httpClient.post("/jobRegistration/insert",job);
}
showAppliedJobs(username:string)
 {
  console.log(username)
  return this.httpClient.get("/AppliedJobs/search/"+username);
 }

}
