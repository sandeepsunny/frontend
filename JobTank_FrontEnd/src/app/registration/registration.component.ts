import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { User } from 'src/User';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit 
{
  user: User;
  disp_msg!: String;

  constructor(
    private AuthenticationService: AuthenticationService,
    private router: Router
  ) {
    this.user = new User();
  }

  
  ngOnInit(): void {}

  register() {
    console.log(this.user);
    this.AuthenticationService.registerUser(this.user).subscribe({
      next: (data) => {
        this.disp_msg =
          'Congratulations ' +
          this.user.userName +
          ' your account created successfully';
          Swal.fire('Congratulations 🥳🎉', this.user.userName + ' your account created successfully', 'success');


        this.router.navigate(['/showjobs']);
      },
      error: (e) => {
        console.log(e);
        Swal.fire('Failed to create account ! ', this.user.userName + '  Already Exists with this Username', 'error');
      },
    });
  }
}
