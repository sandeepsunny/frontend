import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/User';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-showappliedjobs',
  templateUrl: './showappliedjobs.component.html',
  styleUrls: ['./showappliedjobs.component.css']
})
export class ShowappliedjobsComponent implements OnInit
{
  job:any;
 username:any
 id:any
  ngOnInit(): void {
   this.showAppliedJobs();
  }
  constructor(private auth:AuthenticationService, private router:Router ){}
  showAppliedJobs()
  {
     this.username=localStorage.getItem("username");
      console.log(this.username)
       this.auth.showAppliedJobs(this.username).subscribe((data:any) => {
        console.log(data);
       this.job=data;
       });
  }
  cancelJob()
  {
    
     this.id=localStorage.getItem("id");
      console.log(this.id);
       this.auth.cancelJob(this.id).subscribe((data:any) => {
        console.log(this.id);
       this.job=data;
       });
  }

}