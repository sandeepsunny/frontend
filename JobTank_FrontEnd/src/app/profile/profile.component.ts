import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/User';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  user:User;
  username:any;
  disp_msg:string | undefined;
  

  constructor(private authenticationService: AuthenticationService,private router:Router,private toaster:ToastrService) { 
    this.user=new User();
  }

  ngOnInit(): void {
    // this.getProfile();
   
    
  }
  showToaster5(){
    
}
  updateProfile(){
    
    console.log(this.user);
   // this.username=localStorage.getItem("username");
    this.authenticationService.updateProfile(this.user).subscribe({
      
      next: (data: any) => {
        if(data==null){
          this.toaster.error('Sorry username is not valid')
          
          // this.disp_msg =
          // 'Sorry ' +
          // this.user.userName +
          // ' username  does not exsist or there is some error pls try again';
          
          
        }else{
          this.toaster.success('Profile updated')
         
        // console.log(data)
        // this.disp_msg =
        //   'Congratulations ' +
        //   this.user.userName +
        //   ' profile updated';
        }
        
      },
      // error: (e: any) => {
      //   console.log(e);
      //   this.disp_msg =
      //     'Failed to update!';
      // },
    });

  }

  // getProfile(){
  //   this.authenticationService.getProfile().subscribe({
      
  //     next: (data) => {
  //       // @ts-ignore
  //       this.user.userName=data['userName'];
  //       // @ts-ignore
  //       this.user.email=data['email'];
  //       // @ts-ignore
  //       this.user.qualification=data['qualification'];
  //       console.log(data)
  //       this.disp_msg =
        
  //         '';
  //       ;
  //     },
  //     error: (e) => {
  //       console.log(e);
  //       this.disp_msg =
  //         'data not got';
  //     },
  //   });
  // }

}
