import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Adminpart1Component } from './adminpart1/adminpart1.component';
import { Adminpart2Component } from './adminpart2/adminpart2.component';
import { CategorysearchComponent } from './categorysearch/categorysearch.component';
import { CompanyRegistrationFormComponent } from './company-registration-form/company-registration-form.component';
import { FounderComponent } from './founder/founder.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { Navbar3Component } from './navbar3/navbar3.component';
import { ProfileComponent } from './profile/profile.component';
import { RegistrationComponent } from './registration/registration.component';
import { ShowJobsComponent } from './show-jobs/show-jobs.component';
import { ShowappliedjobsComponent } from './showappliedjobs/showappliedjobs.component';
import { ShowloginjobsComponent } from './showloginjobs/showloginjobs.component';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
  {path:'',component:HomeComponent},

  // {path:'',component:Navbar3Component},
  {path:"login",component:LoginComponent},
  {path:"founder",component:FounderComponent},
  {path:"showloginjobs",component:ShowloginjobsComponent},
  {path:"home",component:HomeComponent},

  {path:"navbar",component:NavbarComponent},
  {path:"category",component:CategorysearchComponent},

  {path:"appliedjobs",component:ShowappliedjobsComponent},
  
   {path:"register",component:RegistrationComponent},
   {path:"company",component:CompanyRegistrationFormComponent},
   {path:"admin2",component:Adminpart2Component},
  {path:"admin1",component:Adminpart1Component},
  {path:"update",component:UpdateComponent},
  {path:"companyRegistration",component:CompanyRegistrationFormComponent},
 // {path:"showjobs",component:ShowJobsComponent},
  {path:"profileUpdate",component:ProfileComponent},
  {path:"showjobs",component:ShowJobsComponent},
//{path:"navbar/showapplied",component:ShowappliedjobsComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
