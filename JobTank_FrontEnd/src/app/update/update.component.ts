import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminCRUDService } from '../admin-crud.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent  implements OnInit{
  id=localStorage.getItem("id");
  name=localStorage.getItem("name");
  location=localStorage.getItem("location");
  company=localStorage.getItem("company");
  role=localStorage.getItem("role");
  ngOnInit(): void {
    
  }
  constructor(private crud:AdminCRUDService,private router:Router,private toaster: ToastrService){}

  showToatr3(){
    this.toaster.warning('Company Updated Successfully','Updated');
    this.router.navigateByUrl("admin1");
  }
  update(data:any)
  {
   data.id=localStorage.getItem("id");
   return this.crud.updateJob(data.id,data).subscribe((c:any)=>
       {
         this.gotolist();
       })
     }
   gotolist() {
     this.router.navigateByUrl("admin");
   }

}
