import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router, RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShowJobsComponent } from './show-jobs/show-jobs.component';
import { RegistrationComponent } from './registration/registration.component';
import { FormsModule } from '@angular/forms';
import { AddDetailsComponent } from './add-details/add-details.component';
import { Adminpart1Component } from './adminpart1/adminpart1.component';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { ToastrModule } from 'ngx-toastr';
import { UpdateComponent } from './update/update.component';
import { CompanyRegistrationFormComponent } from './company-registration-form/company-registration-form.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProfileComponent } from './profile/profile.component';
import { CategorysearchComponent } from './categorysearch/categorysearch.component';
import { LoginComponent } from './login/login.component';
import { ShowappliedjobsComponent } from './showappliedjobs/showappliedjobs.component';
import { HomeComponent } from './home/home.component';
import { Adminpart2Component } from './adminpart2/adminpart2.component';
import { Navbar2Component } from './navbar2/navbar2.component';
import { FounderComponent } from './founder/founder.component';
import { Navbar3Component } from './navbar3/navbar3.component';
import { ShowloginjobsComponent } from './showloginjobs/showloginjobs.component';
import { NgxCaptchaModule } from 'ngx-captcha';

@NgModule({
  declarations: [
    AppComponent,
    ShowJobsComponent,
    RegistrationComponent,
    AddDetailsComponent,
    Adminpart1Component,
    UpdateComponent,
    CompanyRegistrationFormComponent,
    FooterComponent,
    NavbarComponent,
    FooterComponent,
    ProfileComponent,
    CategorysearchComponent,
    LoginComponent,
    ShowappliedjobsComponent,
    HomeComponent,
    Adminpart2Component,
    Navbar2Component,
    FounderComponent,
    Navbar3Component,
    ShowloginjobsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
   // ToastrModule.forRoot()
   ToastrModule.forRoot(),
   NgxCaptchaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
