import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminCRUDService {
  
  
 

  constructor(private httpclient:HttpClient,private router:Router) { 
    
}

addJob(jobs:any)
{
 return  this.httpclient.post("/job/insert",jobs);
}

getAllJobs()
{
  return this.httpclient.get("/job/list");
}
delete1(id: any) {
  return this.httpclient.delete("/job/delete/"+id);
}
updateJob(id:any,job:any)
{
 return this.httpclient.put("/job/update/",job);
}
}