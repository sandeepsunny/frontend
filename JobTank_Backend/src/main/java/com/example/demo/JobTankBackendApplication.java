package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.example.demo.service.EmailService;

@SpringBootApplication
public class JobTankBackendApplication {
	@Autowired
  private  EmailService email;
	
	public static void main(String[] args) {
		SpringApplication.run(JobTankBackendApplication.class, args);
	}
	@EventListener(ApplicationReadyEvent.class)
    public void sendMail() {
        email.sendEmail(
                "jobtank121@gmail.com",
                "Registration SuccessFull",
                "registration");
    }

}
