package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "jobregistration")
public class JobRegistration {
	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "role")
	private String role;
	@Column(name = "company")
	private String company;
	@Column(name = "username")
	private String userName;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name = "email")
	private String email;
	@Column(name = "ssc")
	private String ssc;
	@Column(name = "inter")
	private String inter;
	@Column(name = "degree")
	private String degree;
	JobRegistration(){
		
	}
	public JobRegistration(int id, String role, String company, String userName, String email, String ssc, String inter,
			String degree) {
		super();
		this.id = id;
		this.role = role;
		this.company = company;
		this.userName = userName;
		this.email = email;
		this.ssc = ssc;
		this.inter = inter;
		this.degree = degree;
	}
	
	@Override
	public String toString() {
		return "JobRegistration [id=" + id + ", role=" + role + ", company=" + company + ", userName=" + userName
				+ ", email=" + email + ", ssc=" + ssc + ", inter=" + inter + ", degree=" + degree + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getSsc() {
		return ssc;
	}
	public void setSsc(String ssc) {
		this.ssc = ssc;
	}
	public String getInter() {
		return inter;
	}
	public void setInter(String inter) {
		this.inter = inter;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}

	
	
}
