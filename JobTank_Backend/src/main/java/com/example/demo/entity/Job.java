package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "job")
public class Job{
	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "Catogeryname")
	private String Catogeryname;

	@Column(name = "location")
	private String location;

	@Column(name = "role")
	private String role;
	
	@Column(name = "company")
	private String company;

	
	
	public Job() {

	}
	
	public Job(int id, String name, String location, String role, String company) {
		this.id = id;
		this.Catogeryname = name;
		this.location = location;
		this.role = role;
		this.company = company;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return Catogeryname;
	}

	public void setName(String name) {
		this.Catogeryname = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	

	@Override
	public String toString() {
		return "Job [id=" + id + ", name=" + Catogeryname + ", location=" + location + ", role=" + role + ", company=" + company
				+ "]";
	}
}
