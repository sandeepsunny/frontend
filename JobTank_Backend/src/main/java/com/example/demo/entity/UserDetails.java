package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "userDetails")
public class UserDetails {
	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private int id;
		@Column(name = "username",unique=true)
	    private String userName;
		@Column(name = "email",unique=true)
	    private String email;
		@Column(name = "password")
	    private String password;
		@Column(name = "qualification")
	    private String qualification;
	
		
		public UserDetails(int id, String userName, String email, String password, String qualification) {
			this.id = id;
			this.userName = userName;
			this.email = email;
			this.password = password;
			this.qualification = qualification;
		}

		@Override
		public String toString() {
			return "User [id=" + id + ", userName=" + userName + ", email=" + email + ", password=" + password
					+ ", qualification=" + qualification  + "]";
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getuserName() {
			return userName;
		}

		public void setuserName(String userName) {
			this.userName = userName;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getQualification() {
			return qualification;
		}

		public void setQualification(String qualification) {
			this.qualification = qualification;
		}

		public UserDetails() {
			super();
		}

		public UserDetails(String userName) {
			super();
			this.userName = userName;
			
		}
	

}
/*
 * 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "job")
public class Job{
	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "Catogeryname")
	private String Catogeryname;

	@Column(name = "location")
	private String location;

	@Column(name = "role")
	private String role;
	
	@Column(name = "company")
	private String company;

	
	
	public Job() {

	}
	
	public Job(int id, String name, String location, String role, String company) {
		this.id = id;
		this.Catogeryname = name;
		this.location = location;
		this.role = role;
		this.company = company;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return Catogeryname;
	}

	public void setName(String name) {
		this.Catogeryname = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	

	@Override
	public String toString() {
		return "Job [id=" + id + ", name=" + Catogeryname + ", location=" + location + ", role=" + role + ", company=" + company
				+ "]";
	}
}

 */
