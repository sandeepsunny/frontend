package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AppliedJobs")
public class AppliedJobs{
      @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
      private int id;
      
    @Column(name = "jobid")
    private int jobid;

    @Column(name = "Catogeryname")
    private String Catogeryname;

    @Column(name = "location")
    private String location;

    @Column(name = "role")
    private String role;
    
    @Column(name = "company")
    private String company;
     @Column(name="username")
     private String username;
    
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public AppliedJobs() {

    }
    
    public AppliedJobs(int id, String name, String location, String role, String company,String username,int jobid) {
        this.id = id;
        this.Catogeryname = name;
        this.location = location;
        this.role = role;
        this.company = company;
        this.username=username;
        this.jobid=jobid;
        
    }

    public int getJobid() {
        return jobid;
    }

    public void setJobid(int jobid) {
        this.jobid = jobid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Catogeryname;
    }

    public void setName(String name) {
        this.Catogeryname = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "AppliedJobs [id=" + id + ", jobid=" + jobid + ", Catogeryname=" + Catogeryname + ", location="
                + location + ", role=" + role + ", company=" + company + ", username=" + username + "]";
    }



    
}
