package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Job;
import com.example.demo.entity.UserDetails;
import com.example.demo.service.JobServiceImplementation;

@RestController
@RequestMapping("/job")
public class JobController 
{
	private JobServiceImplementation src;

	@Autowired
	public JobController(JobServiceImplementation src) {
		this.src = src;
	}
	
	@GetMapping("/list")
	public List<Job> displayAll() {
		return src.displayAll();
	}


	@PostMapping("/insert") 
	public void insert(@RequestBody Job jb) {
		src.insertJob(jb);
	}

	@PutMapping("/update") 
	public void update(@RequestBody Job jb) {
		src.updateJob(jb);
	}

	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") int id)
	{
		src.deleteJob(id);
	}
	@GetMapping("/search/{Catogeryname}")
	public List<Job> updateProfile(@PathVariable("Catogeryname") String name) {
		
			return src.searchBycategory(name);
			
		
	}
	
}