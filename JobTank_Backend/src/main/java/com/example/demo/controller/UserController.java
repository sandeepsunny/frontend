package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.UserDetails;
import com.example.demo.service.EmailService;
import com.example.demo.service.UserServiceImplementation;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins="http://localhost:4200")
public class UserController {
	private UserServiceImplementation ser;
	
	@Autowired
	private  EmailService email;
	public UserController(UserServiceImplementation ser) {
		this.ser=ser;
	}
	@GetMapping("/list")
	public List<UserDetails> displayAll() {
		return ser.displayAll();
	}


	@PostMapping("/list") 
	public void insert(@RequestBody UserDetails user) {
		ser.insert(user);
	}

	@PutMapping("/update") 
	public void update(@RequestBody UserDetails user) {
		ser.insert(user);
	  
	}

	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") int id)
	{
		ser.delete(id);
	}
	@PutMapping("/update/{userName}")
	public UserDetails updateProfile(@PathVariable("userName") String userName,@RequestBody UserDetails user) {
		
			return ser.updateProfile(user, userName);
			
		
	}
	@PostMapping("/login")
    public ResponseEntity<UserDetails> loginUser(@RequestBody UserDetails userData){
        System.out.println(userData);
        UserDetails user=ser.findByUserName(userData.getuserName());
        if(user != null) {
            System.out.println(user);
        if(user.getPassword().equals(userData.getPassword())) 
            
            return ResponseEntity.ok(user);
        }
        //return (ResponseEntity<User>) ResponseEntity.internalServerError();
        return ResponseEntity.ok(null);
        
    }
	@GetMapping("/email/{username}")
	public void email(@PathVariable("username") String username) {
		UserDetails user=ser.findByUserName(username);
		System.out.println(user);
		email.sendEmail(user.getEmail(),"Selected","COngratulations");
		System.out.println("EMail sent ");
		
	}
	@GetMapping("/email1/{username}")
	public void emailReject(@PathVariable("username") String username) {
		UserDetails user=ser.findByUserName(username);
		System.out.println(user);
		String x="sunny";
		email.sendEmail(user.getEmail(),x+"Rejected","sorry");
		System.out.println("EMail sent ");
		
	}
}

