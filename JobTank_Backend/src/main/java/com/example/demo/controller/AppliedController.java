package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.AppliedJobs;
import com.example.demo.service.AppliedServiceImplementation;

@RestController
@RequestMapping("/AppliedJobs")
public class AppliedController {
	
	private AppliedServiceImplementation ser;
	
	@Autowired
	public AppliedController(AppliedServiceImplementation ser) {
		this.ser=ser;
	}
	
	@GetMapping("/list")
	public List<AppliedJobs> displayAll() {
		return ser.display();
	}
	
	@PostMapping("/list1")
	public void insert(@RequestBody AppliedJobs j) {
		ser.insert(j);
	}
	@GetMapping("/search/{username}")
    public List<AppliedJobs> searchName(@PathVariable("username") String username)
    {
        List<AppliedJobs> aj=ser.searchByName(username);
        if(aj==null)
            throw new RuntimeException("No username is found with the given name .!");
        return aj;
        
    }
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") int id) {
	ser.delete(id);
	}
}


/*
 * 
@RestController
@RequestMapping("/job")
public class JobController 
{
	private JobServiceImplementation src;

	@Autowired
	public JobController(JobServiceImplementation src) {
		this.src = src;
	}
	
	@GetMapping("/list")
	public List<Job> displayAll() {
		return src.displayAll();
	}


	@PostMapping("/insert") 
	public void insert(@RequestBody Job jb) {
		src.insertJob(jb);
	}

	@PutMapping("/update") 
	public void update(@RequestBody Job jb) {
		src.updateJob(jb);
	}

	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") int id)
	{
		src.deleteJob(id);
	}

//	public List<Employee> searchByEmpName(String name) {
//		return null;
//	}	
}
 */
