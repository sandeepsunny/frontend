package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.entity.JobRegistration;
import com.example.demo.entity.UserDetails;
import com.example.demo.service.EmailService;
import com.example.demo.service.JobRegistrationServiceImplimentation;
import com.example.demo.service.JobRegistrationServiceInterface;

@RestController
@RequestMapping("/jobRegistration")
public class JobRegistrationController {
	
	private JobRegistrationServiceImplimentation service;
	@Autowired
	private  EmailService email;
	
	@Autowired
	public JobRegistrationController(JobRegistrationServiceImplimentation ser){
		this.service=ser;
	}
	@GetMapping("/list")
	public List<JobRegistration> displayAll() {
		return service.display();
	}


	@PostMapping("/insert") 
	public void insert(@RequestBody JobRegistration obj) {
		service.insert(obj);
	}
	@GetMapping("/email/{id}")
	public void email(@PathVariable("id") int id) {
		JobRegistration user=service.findById(id);
		String msg="Hi,"+user.getUserName()+" we are very happy to inform you that you got shortlisted for the "+user.getCompany()+" we will update you about the further rounds thanks for applying at jobtank";
		System.out.println(user);
		email.sendEmail(user.getEmail(),"Update of Application at Jobtank",msg);
		System.out.println("EMail sent ");
		
	}
	@GetMapping("/email1/{id}")
	public void emailReject(@PathVariable("id") int id) {
		JobRegistration user=service.findById(id);
		String msg="Hi,"+user.getUserName()+" we regret to inform you that your not selected for the comapny "+user.getCompany()+".Thank you for your intrest.";
		System.out.println(user);
		email.sendEmail(user.getEmail(),"Update of Application at Jobtank",msg);
		System.out.println("EMail sent ");
		
	}

}
