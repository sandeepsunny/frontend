package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.UserDetails;

public interface UserServiceDecleration 
{
	public List<UserDetails> displayAll();
	public void insert(UserDetails user);
	public void update(UserDetails user);
	public void delete(int id);
	public UserDetails updateProfile(UserDetails user,String userName);
	public UserDetails findByUserName(String getuserName);
}
