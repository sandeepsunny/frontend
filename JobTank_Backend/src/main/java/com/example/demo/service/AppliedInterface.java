package com.example.demo.service;

import java.util.List;
import com.example.demo.entity.*;
public interface AppliedInterface {
	public List<AppliedJobs> display();
	public void insert(AppliedJobs j);
	public List<AppliedJobs> searchByName(String username);
	public void delete(int id);
}
