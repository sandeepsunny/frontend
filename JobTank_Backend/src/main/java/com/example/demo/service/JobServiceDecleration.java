package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Job;
import com.example.demo.entity.JobRegistration;
import com.example.demo.entity.UserDetails;

public interface JobServiceDecleration 
{
	public List<Job> displayAll();
	public void insertJob(Job jb);
	public void updateJob(Job jb);
	public void deleteJob(int id);
	public List<Job> searchBycategory(String name);
	
}
