package com.example.demo.service;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.UserDetails;
import com.example.demo.persistance.UserRepository;


@Service
public class UserServiceImplementation implements UserServiceDecleration{
	UserRepository p;
	@Autowired
	public UserServiceImplementation(UserRepository p) {
		this.p=p;
	}
	@Override
	@Transactional
	public List<UserDetails> displayAll() {
		
		return p.findAll();
	}
	@Override
	@Transactional
	public void insert(UserDetails user) {
		 p.save(user);
		
	}
	@Override
	@Transactional
	public void update(UserDetails user) {
		// TODO Auto-generated method stub
		p.save(user);
		
	}
	@Override
	@Transactional
	public void delete(int id) {
		p.deleteById(id);
		// TODO Auto-generated method stub
		
	}
	/*
	 *  public User updateProfile(User user,String userName) throws UserServicesException {
    User exsistinguser = userRepository.findByUserName(user.getuserName());
    if(exsistinguser!=null){
//         exsistinguser.copyWithoutNotNull(user);
        return userRepository.save(exsistinguser);
    }
    return null;
    }
	 */
	@Override
	@Transactional
	public UserDetails updateProfile(UserDetails user, String userName) {
		UserDetails exsistingUser=p.findByUserName(userName);
		if(exsistingUser != null) {
			exsistingUser.setEmail(user.getEmail());
			exsistingUser.setQualification(user.getQualification());
		 return  p.save(exsistingUser);
		  // System.out.println("Successfull");
		 
		}
		return null;
	}
	@Override
	@Transactional
	public UserDetails findByUserName(String getuserName) {
	
		return p.findByUserName(getuserName);
	}



}