package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.AppliedJobs;
import com.example.demo.persistance.AppliedJobInterface;
 @Service
public class AppliedServiceImplementation implements AppliedInterface{
	private AppliedJobInterface p;
	@Autowired
	public AppliedServiceImplementation(AppliedJobInterface p) {
		this.p=p;
	}
	@Override
	@Transactional
	public List<AppliedJobs> display() {
		
		
		return p.findAll();
	}
	@Override
	@Transactional
	public void insert(AppliedJobs j) {
		p.save(j);
		
	}
	@Override
	@Transactional
	public List<AppliedJobs> searchByName(String username) {
		// TODO Auto-generated method stub
		 return p.findByUserName(username);
	}
	@Override
	public void delete(int id) {
		p.deleteById(id);
		
	}

	

}
