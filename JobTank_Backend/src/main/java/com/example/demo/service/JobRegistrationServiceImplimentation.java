package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.JobRegistration;
import com.example.demo.persistance.JobRegistrationRepository;
@Service
public class JobRegistrationServiceImplimentation implements JobRegistrationServiceInterface{
	@Autowired
	JobRegistrationRepository p;
	@Override
	@Transactional
	public List<JobRegistration> display() {
		
		return p.findAll();
	}

	@Override
	@Transactional
	public void insert(JobRegistration obj) {
		p.save(obj);
		
	}

	@Override
	public JobRegistration findById(int id) {
		// TODO Auto-generated method stub
		return p.findById(id);
	}

}
