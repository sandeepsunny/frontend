package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Job;
import com.example.demo.entity.JobRegistration;
import com.example.demo.persistance.JobRepository;

@Service
public class JobServiceImplementation implements JobServiceDecleration
{
	private JobRepository jbrep;
	
	
	@Autowired
	public JobServiceImplementation(JobRepository jbrep) {
		this.jbrep = jbrep;
	}


	@Override
	@Transactional
	public List<Job> displayAll()
	{
		return jbrep.findAll();
	}
	
	@Override
	@Transactional
	public void insertJob(Job jb) 
	{
		jbrep.save(jb);

	}

	@Override
	@Transactional
	public void updateJob(Job jb)
	{
		jbrep.save(jb);
	}

	@Override
	@Transactional
	public void deleteJob(int id)
	{
		jbrep.deleteById(id);
	}


	@Override
	@Transactional
	public List<Job> searchBycategory(String name) {
		
		return jbrep.findByname(name);
	}


	
}
