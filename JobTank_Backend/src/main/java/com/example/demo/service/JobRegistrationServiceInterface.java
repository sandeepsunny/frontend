package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.JobRegistration;



public interface JobRegistrationServiceInterface {
  public List<JobRegistration> display();
	public void insert(JobRegistration obj);
	public JobRegistration findById(int id);
}
