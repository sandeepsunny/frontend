package com.example.demo.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.AppliedJobs;

@Repository
public interface AppliedJobInterface extends JpaRepository<AppliedJobs,Integer> {
	@Query("from AppliedJobs  where username=?1")
    List<AppliedJobs> findByUserName(String query);
  
}
