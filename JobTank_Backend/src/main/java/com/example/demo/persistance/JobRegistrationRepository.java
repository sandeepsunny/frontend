package com.example.demo.persistance;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.demo.entity.JobRegistration;


public interface JobRegistrationRepository extends JpaRepository<JobRegistration, Integer>{
	JobRegistration findById(int id);
}
