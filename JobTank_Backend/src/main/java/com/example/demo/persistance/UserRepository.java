package com.example.demo.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.UserDetails;

public interface UserRepository extends JpaRepository<UserDetails, Integer>
{

    UserDetails findByUserName(String userName);

    UserDetails findByUserNameAndPassword(String userName, String password);
}
